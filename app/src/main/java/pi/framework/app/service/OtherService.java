package pi.framework.app.service;

import pi.framework.core.stereotype.Service;

@Service
public class OtherService {
    public String dummyResponse(){
        return "dummy data";
    }
}
