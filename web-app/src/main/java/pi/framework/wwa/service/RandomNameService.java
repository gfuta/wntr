package pi.framework.wwa.service;

import pi.framework.core.stereotype.Service;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class RandomNameService {

    private final List<String> names = Arrays.asList("Waylon Dalton",
            "Justine Henderson",
            "Abdullah Lang",
            "Marcus Cruz",
            "Thalia Cobb",
            "Mathias Little",
            "Eddie Randolph",
            "Angela Walker",
            "Waylon Dalton",
            "Justine Henderson",
            "Abdullah Lang",
            "Marcus Cruz",
            "Thalia Cobb",
            "Mathias Little",
            "Eddie Randolph",
            "Angela Walker",
            "Lia Shelton",
            "Hadassah Hartman",
            "Joanna Shaffer",
            "Jonathon Sheppard");

    public String generate() {
        int randomNum = ThreadLocalRandom.current().nextInt(0, names.size()-1);
        return names.get(randomNum);
    }
}
