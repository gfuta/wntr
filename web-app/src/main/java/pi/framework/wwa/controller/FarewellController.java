package pi.framework.wwa.controller;

import pi.framework.core.annotation.PowerInject;
import pi.framework.core.stereotype.mvc.Controller;
import pi.framework.core.stereotype.mvc.WebMethod;
import pi.framework.core.web.protocol.ContentType;
import pi.framework.wwa.service.RandomNameService;

@Controller
public class FarewellController {

    @PowerInject
    private RandomNameService randomNameService;

    @WebMethod(path = "/farewell",
            responseContentType = ContentType.TEXT_HTML)
    public String echo() {
        return "Farewell, to you <b>" + randomNameService.generate() + "</b>";
    }

    @WebMethod(path = "/hastaLaVista",
            responseContentType = ContentType.TEXT_HTML)
    public String hastaLaVista() {
        return "Hasta la vista, baby";
    }
}
