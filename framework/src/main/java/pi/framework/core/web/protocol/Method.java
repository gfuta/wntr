package pi.framework.core.web.protocol;

/**
 * Class representing HTTP request methods
 */
public enum Method {
    GET, POST, PUT, DELETE,

    PATCH, OPTIONS, HEAD
}
