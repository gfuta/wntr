package pi.framework.core.web;

import pi.framework.core.web.protocol.ContentType;

import java.util.Objects;

public class RunMethodDescriptor {
    private Object instance;
    private String method;
    private ContentType contentType;

    Object getInstance() {
        return instance;
    }

    String getMethod() {
        return method;
    }

    ContentType getContentType() {
        return contentType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RunMethodDescriptor that = (RunMethodDescriptor) o;
        return Objects.equals(instance, that.instance) &&
                Objects.equals(method, that.method) &&
                contentType == that.contentType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(instance, method, contentType);
    }

    static final class Builder {
        Object instance;
        String method;
        ContentType contentType;

        private Builder() {
        }

        static Builder get() {
            return new Builder();
        }

        Builder withInstance(Object instance) {
            this.instance = instance;
            return this;
        }

        Builder withMethod(String method) {
            this.method = method;
            return this;
        }

        Builder withContentType(ContentType contentType) {
            this.contentType = contentType;
            return this;
        }

        RunMethodDescriptor build() {
            RunMethodDescriptor runMethodDescriptor = new RunMethodDescriptor();
            runMethodDescriptor.contentType = this.contentType;
            runMethodDescriptor.instance = this.instance;
            runMethodDescriptor.method = this.method;
            return runMethodDescriptor;
        }
    }
}
