package pi.framework.core.web;

import pi.framework.core.bean.BeanDefinitionInfo;
import pi.framework.core.context.ApplicationContext;
import pi.framework.core.context.ContextInitializationException;
import pi.framework.core.context.PiApplicationContext;
import pi.framework.core.stereotype.mvc.Controller;
import pi.framework.core.stereotype.mvc.WebMethod;
import pi.framework.core.web.protocol.Method;

import javax.servlet.ServletConfig;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import java.util.*;
import java.util.stream.Stream;

final public class FrontControllerServlet extends HttpServlet {

    private final Map<Method, Map<String, RunMethodDescriptor>> dispatchingConfig = new EnumMap(Method.class);

    @Override
    public void init(ServletConfig config) {
        String applicationContextClassName = config.getInitParameter("applicationContextClass");
        ApplicationContext applicationContext = initPiContext(applicationContextClassName);

        // fetch @Controller components and initialize dispatcher configuration
        Set<BeanDefinitionInfo> controllerBeanDefinitions = applicationContext.getBeansDefinitionOfType(Controller.class);
        Stream.of(Method.values()).forEach(method -> dispatchingConfig.put(method, new HashMap<>()));

        // configure dispatcher with PiFrameworkBeans
        controllerBeanDefinitions.forEach(this::registerWebMethods);
    }

    private void registerWebMethods(BeanDefinitionInfo beanDefinitionInfo) {
        Class clazz = beanDefinitionInfo.getClazz();

        Stream.of(clazz.getDeclaredMethods()).forEach(method -> {
            WebMethod webMethodAnnotation = method.getAnnotation(WebMethod.class);
            dispatchingConfig.get(webMethodAnnotation.method())
                    .put(webMethodAnnotation.path(), RunMethodDescriptor.Builder.get()
                            .withMethod(method.getName())
                            .withContentType(webMethodAnnotation.responseContentType())
                            .withInstance(beanDefinitionInfo.getInstance())
                            .build()
                    );
        });
    }

    private ApplicationContext initPiContext(String applicationContextClassName) {
        Class applicationContextClass;
        try {
            applicationContextClass = Class.forName(applicationContextClassName);
            return new PiApplicationContext(applicationContextClass);
        } catch (ClassNotFoundException e) {
            System.err.println("The <init-param> of name 'applicationContextClass' has to be defined");
            e.printStackTrace();
        }
        throw new ContextInitializationException(applicationContextClassName);
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doProcess(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doProcess(req, resp);
    }

    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doProcess(req, resp);
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doProcess(req, resp);
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        doProcess(req, resp);

    }

    private void doProcess(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        ServletOutputStream out = resp.getOutputStream();
        Map<String, RunMethodDescriptor> dispatchingConfigPathMap = dispatchingConfig.get(Method.valueOf(req.getMethod()));
        RunMethodDescriptor runMethodDelegateDescriptor = dispatchingConfigPathMap.get(req.getRequestURI());

        if (runMethodDelegateDescriptor != null) {
            resp.setContentType(runMethodDelegateDescriptor.getContentType().getValue());
            out.print((String) invokePiFrameworkControllerMethod(runMethodDelegateDescriptor, resp));
        } else {
            resp.setContentType("text/html");
            resp.setCharacterEncoding("UTF-8");
            resp.setStatus(HttpServletResponse.SC_NOT_FOUND);
            out.print(String.format("UPSS... nothing to do with %s request on '%s' path (URI).<br />Sayonara!", req.getMethod(), req.getRequestURI()));
        }
    }

    private Object invokePiFrameworkControllerMethod(RunMethodDescriptor runMethodDescriptor, HttpServletResponse resp) {
        Object controllerInstance = runMethodDescriptor.getInstance();
        try {
            java.lang.reflect.Method controllerMethod = controllerInstance
                    .getClass().getMethod(runMethodDescriptor.getMethod(), null); // sic! sad null param
            return controllerMethod.invoke(controllerInstance); // tu też słabo, ale na cele dydaktyczne wystarczy
        } catch (Exception e) {
            resp.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
        return "";
    }

}
