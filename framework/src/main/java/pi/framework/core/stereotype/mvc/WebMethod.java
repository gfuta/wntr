package pi.framework.core.stereotype.mvc;


import pi.framework.core.web.protocol.ContentType;
import pi.framework.core.web.protocol.Method;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface WebMethod {

    String path() default "/";
    Method method() default Method.GET;
    ContentType responseContentType() default ContentType.TEXT_PLAIN;

}
