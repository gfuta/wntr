package pi.framework.core.context;

import pi.framework.core.bean.BeanDefinitionInfo;

import java.util.Set;

public interface ApplicationContext {
    <T> T getBean(Class<T> clazz);
    Set<BeanDefinitionInfo> getBeansDefinitionOfType(Class clazz);
}
