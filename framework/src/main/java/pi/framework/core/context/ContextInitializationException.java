package pi.framework.core.context;

public class ContextInitializationException extends RuntimeException {
    public ContextInitializationException(String className) {
        super("Can not initialize context for class: " + className);
    }
}
