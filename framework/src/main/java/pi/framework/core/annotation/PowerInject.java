package pi.framework.core.annotation;


import java.lang.annotation.*;

@Target({ElementType.FIELD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface PowerInject {

    boolean value() default true;

}
