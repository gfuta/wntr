package pi.framework.nodiapp.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pi.framework.nodiapp.persistence.PersonsDatabaseMock;
import pi.framework.nodiapp.repository.PersonRepository;

class ImportantBusinessServiceTest {

    private PersonsDatabaseMock personsDatabaseMock;
    private PersonRepository personRepository;
    private OtherService otherService;
    private ImportantBusinessService importantBusinessService;

    @BeforeEach
    void setup() {
        personsDatabaseMock = new PersonsDatabaseMock();
        personRepository = new PersonRepository(personsDatabaseMock);
        otherService = new OtherService();
        importantBusinessService = new ImportantBusinessService(personRepository, otherService);
    }

    @Test
    void shouldFetchDefaultPerson() {
        //TODO implement test
    }

    @Test
    void shouldFetchExistingPerson() {
        //TODO implement test
    }

    @Test
    void shouldReturnDummyResponse() {
        //TODO implement test
    }

    @Test
    void shouldFetchPersonsWithGivenName() {
        //TODO implement test
    }

}
