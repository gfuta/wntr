package pi.framework.nodiapp;

import pi.framework.nodiapp.dto.PersonDTO;
import pi.framework.nodiapp.persistence.PersonsDatabaseMock;
import pi.framework.nodiapp.repository.PersonRepository;
import pi.framework.nodiapp.service.ImportantBusinessService;
import pi.framework.nodiapp.service.OtherService;

import java.util.List;

public class PiDemoApplication {

    private PiDemoApplication(){}

    public static void main(String[] args) {
        System.out.println("PI framework demo application...");

        PersonsDatabaseMock personsDatabaseMock = new PersonsDatabaseMock();
        PersonRepository personRepository = new PersonRepository(personsDatabaseMock);
        OtherService otherService = new OtherService();
        ImportantBusinessService someImportantService = new ImportantBusinessService(personRepository, otherService);

        PersonDTO randomImportantPerson = someImportantService.fetchRandomImportantPerson();
        System.out.println("RANDOM IMPORTANT PERSON");
        System.out.println(randomImportantPerson);

        PersonDTO personByID = someImportantService.fetchByID(4);
        System.out.println("OSOBA PO NUMERZE ID");
        System.out.println(personByID);

        PersonDTO johnDoe = someImportantService.fetchByID(4);
        System.out.println("NO-NAME");
        System.out.println(johnDoe);

        List<PersonDTO> marlins = someImportantService.fetchByName("Marlin");
        System.out.println("MARLINS");
        marlins.forEach(System.out::println);

        System.out.println(someImportantService.dummyMethod());

        System.out.println("PI framework demo application... finished");

    }

}
