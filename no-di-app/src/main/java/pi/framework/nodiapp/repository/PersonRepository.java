package pi.framework.nodiapp.repository;

import pi.framework.nodiapp.dto.PersonDTO;
import pi.framework.nodiapp.persistence.PersonsDatabaseMock;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class PersonRepository {

    private final PersonsDatabaseMock personsDatabase;

    public PersonRepository(PersonsDatabaseMock personsDatabase) {
        this.personsDatabase = personsDatabase;
    }

    public List<PersonDTO> getPersonsByName(String name) {
        return personsDatabase.getDataStream()
                .filter(person -> person.getName().equalsIgnoreCase(name))
                .collect(Collectors.toList());
    }

    public Optional<PersonDTO> getById(Integer id) {
        return personsDatabase.getDataStream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }

    public PersonDTO getByIdx(int idx) {
        return personsDatabase.get(idx);
    }

    public int countAllPersons() {
        return personsDatabase.size();
    }
}
