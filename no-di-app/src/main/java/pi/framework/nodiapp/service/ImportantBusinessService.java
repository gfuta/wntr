package pi.framework.nodiapp.service;

import pi.framework.nodiapp.dto.PersonDTO;
import pi.framework.nodiapp.repository.PersonRepository;

import java.util.List;
import java.util.Random;

public class ImportantBusinessService {

    private static final Integer DEFAULT_ID = 0;
    private static final String DEFAULT_NAME = "Thomas";
    private static final String DEFAULT_SURNAME = "Anderson";
    private static final String DEFAULT_INFO = "He is The One";
    private static final PersonDTO DEFAULT_PERSON = new PersonDTO(DEFAULT_ID, DEFAULT_NAME, DEFAULT_SURNAME, DEFAULT_INFO);

    private final PersonRepository personRepository;
    private final OtherService otherService;

    private final Random random;

    public ImportantBusinessService(PersonRepository personRepository, OtherService otherService) {
        this.personRepository = personRepository;
        this.otherService = otherService;
        random = new Random(System.currentTimeMillis());
    }

    public PersonDTO fetchRandomImportantPerson() {
        int randomIndex = random.nextInt(personRepository.countAllPersons());

        return personRepository.getByIdx(randomIndex);
    }

    public PersonDTO fetchByID(Integer id) {
        return personRepository.getById(id).orElse(DEFAULT_PERSON);
    }

    public List<PersonDTO> fetchByName(String name) {
        return personRepository.getPersonsByName(name);
    }

    public String dummyMethod() {
        return "RESPONSE: " + otherService.dummyResponse();
    }
}
