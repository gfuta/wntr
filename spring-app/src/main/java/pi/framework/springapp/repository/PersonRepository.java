package pi.framework.springapp.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import pi.framework.springapp.dto.PersonDTO;
import pi.framework.springapp.persistence.PersonsDatabaseMock;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Repository
public class PersonRepository {

    @Autowired
    private PersonsDatabaseMock personsDatabase;

    public List<PersonDTO> getPersonsByName(String name) {
        return personsDatabase.getDataStream()
                .filter(person -> person.getName().equalsIgnoreCase(name))
                .collect(Collectors.toList());
    }

    public Optional<PersonDTO> getById(Integer id) {
        return personsDatabase.getDataStream()
                .filter(person -> person.getId().equals(id))
                .findFirst();
    }

    public PersonDTO getByIdx(int idx) {
        return personsDatabase.get(idx);
    }

    public int countAllPersons() {
        return personsDatabase.size();
    }
}
