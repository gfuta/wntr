package pi.framework.springapp.service;

import org.springframework.stereotype.Service;

@Service
public class OtherService {
    public String dummyResponse(){
        return "dummy data";
    }
}
