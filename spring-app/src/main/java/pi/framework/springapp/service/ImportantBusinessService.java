package pi.framework.springapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pi.framework.springapp.dto.PersonDTO;
import pi.framework.springapp.repository.PersonRepository;

import java.util.List;
import java.util.Random;

@Service
public class ImportantBusinessService {

    private static final Integer DEFAULT_ID = 0;
    private static final String DEFAULT_NAME = "Thomas";
    private static final String DEFAULT_SURNAME = "Anderson";
    private static final String DEFAULT_INFO = "He is The One";
    private static final PersonDTO DEFAULT_PERSON = new PersonDTO(DEFAULT_ID, DEFAULT_NAME, DEFAULT_SURNAME, DEFAULT_INFO);

    @Autowired
    private PersonRepository personRepository;

    @Autowired
    private OtherService otherService;

    private final Random random;

    public ImportantBusinessService() {
        random = new Random(System.currentTimeMillis());
    }

    public PersonDTO fetchRandomImportantPerson() {
        int randomIndex = random.nextInt(personRepository.countAllPersons());

        return personRepository.getByIdx(randomIndex);
    }

    public PersonDTO fetchByID(Integer id) {
        return personRepository.getById(id).orElse(DEFAULT_PERSON);
    }

    public List<PersonDTO> fetchByName(String name) {
        return personRepository.getPersonsByName(name);
    }

    public String dummyMethod() {
        return "RESPONSE: " + otherService.dummyResponse();
    }
}
