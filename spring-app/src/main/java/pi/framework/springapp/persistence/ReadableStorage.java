package pi.framework.springapp.persistence;


import java.util.stream.Stream;

interface ReadableStorage<D> {
    Stream<D> getDataStream();

    D get(int id);

    int size();
}
